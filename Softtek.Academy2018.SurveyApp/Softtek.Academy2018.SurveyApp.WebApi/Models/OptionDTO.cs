﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Softtek.Academy2018.SurveyApp.WebApi.Models
{
    public class OptionDTO
    {
        public string Text { get; set; }

        public DateTime CreateDate { get; set; }
    }
}