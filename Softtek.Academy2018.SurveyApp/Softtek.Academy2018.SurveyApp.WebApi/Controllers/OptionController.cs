﻿using Softtek.Academy2018.SurveyApp.Business.Implementation;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebApi.Controllers
{
    [RoutePrefix("api/option")]
    public class OptionController : ApiController
    {
        private readonly OptionService _optionService;

        //class constructor
        public OptionController(OptionService optionService)
        {
            _optionService = optionService;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult CreateOption([FromBody] OptionDTO optionDTO)
        {
            if (optionDTO == null) return BadRequest("Null request :(");

            Option option = new Option
            {
                Text = optionDTO.Text,
                CreatedDate = optionDTO.CreateDate                
            };

            int id = _optionService.Add(option);

            if (id <= 0) return BadRequest("Bad request :(");

            var res = new { optionId = id };

            return Ok(res);            
        }
    }
}
