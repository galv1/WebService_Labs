﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class SurveyDataRepository : ISurveyRepository
    {
        public int Add(Survey survey)
        {
            using (var ctx = new SurveyDbContext())
            {
                ctx.Surveys.Add(survey);
                ctx.SaveChanges();

                return survey.Id;
            }
        }

        public bool Delete(Survey survey)
        {
            throw new NotImplementedException();
        }

        public Survey Get(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Surveys.FirstOrDefault(s => s.Id == id);
            }
        }

        public ICollection<Survey> GetTopTen()
        {
            throw new NotImplementedException();
        }

        public Survey SearchById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Survey survey)
        {
            throw new NotImplementedException();
        }
    }
}
