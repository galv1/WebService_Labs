﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Collections.Generic;

namespace Softtek.Academy2018.SurveyApp.Data.Contracts
{
    public interface IOptionRepository : IGenericRepository<Option>
    {
        //no se repite en la tabla options
        //Una opción no puede ser modificada si es parte de una pregunta
        ICollection<Option> GetAll();

        bool Exist(int id);

        bool ExistInQuestion(Option option);
    }
}
