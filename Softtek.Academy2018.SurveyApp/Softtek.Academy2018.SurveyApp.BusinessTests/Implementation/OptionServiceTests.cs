﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Softtek.Academy2018.SurveyApp.Business;
using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Implementation.Tests
{
    [TestClass()]
    public class OptionServiceTests
    {
        [TestMethod()]
        public void OptionServiceTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void AddTest()
        {
            int expected = 1;

            Mock<IOptionRepository> repo = new Mock<IOptionRepository>();

            repo.Setup(x => x.Exist(It.IsAny<int>())).Returns(false);

            repo.Setup(x => x.Add(It.IsAny<Option>())).Returns(1);

            IOptionService service = new OptionService(repo.Object);

            Option opt = new Option
            {                
                Text = "Anita lava la tina",
                CreatedDate = new DateTime(2018, 02, 21)
            };

            int result = service.Add(opt);

            Assert.AreEqual(expected, result);
        }

        [TestMethod()]
        public void GetAllTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}